<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> <dec:title default="blogit.com" /></title>

  <!-- Bootstrap core CSS -->
  <link href="<c:url value = '/template/web/vendor/bootstrap/css/bootstrap.min.css' />" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<c:url value = '/template/web/vendor/fontawesome-free/css/all.min.css' />" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href=" <c:url value = '/template/web/css/clean-blog.min.css' />" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
	<%@ include file="/common/web/header.jsp" %>
  <!-- /Navigation -->

  <!-- Page Header -->
  <header class="masthead" style = "background-image: url('<c:url value = '/template/web/img/home-bg.jpg' />')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Language is transient, algorithm is eternal</h2>
            <span class="subheading">(nguồn : sưu tầm)</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <dec:body/>
  </div>

  <hr>

  <!-- Footer -->
	<%@ include file="/common/web/footer.jsp" %>
  <!-- /Footer -->

  <!-- Bootstrap core JavaScript -->
  <script src=" <curl value = '/template/web/vendor/jquery/jquery.min.js' />"></script>
  <script src=" <c:url value = '/template/web/vendor/bootstrap/js/bootstrap.bundle.min.js' />"></script>

  <!-- Custom scripts for this template -->
  <script src=" <c:url value = 'template/web/js/clean-blog.min.js' />"></script>

</body>

</html>
