<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
	<div class="container">
		<a class="navbar-brand" href="<c:url value="/home"/>">Oaiker It Blogger</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			Menu <i class="fas fa-bars"></i>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<c:forEach var="item" items="${categorys}">
					<li class="nav-item"><a class="nav-link"
						href="<c:url value="/${item.code}"/> ">${item.name} </a></li>
				</c:forEach>
				<c:if test="${not empty userModelSession}">
					<li class="nav-item"><a class="nav-link"
						href="#">Well come , ${userModelSession.fullName}</a></li>
					<li class="nav-item"><a class="nav-link"
						href="<c:url value="/oaklogout?action=logout"/> ">Thoát </a></li>
				</c:if>
 				<c:if test="${empty userModelSession}">
					<li class="nav-item"><a class="nav-link"
						href="<c:url value="/oaklogin?action=login"/> ">Đăng nhập </a></li>
				</c:if> 
			</ul>
		</div>
	</div>
</nav>