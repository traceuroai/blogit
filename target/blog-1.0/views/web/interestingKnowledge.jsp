<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>

	<div class="row">
		<div class="col-lg-12 col-md-10 mx-auto">
			<hr>
			<!-- Page Content -->
			<div class="container">

				<!-- Call to Action Well -->
				<div class="card text-white bg-secondary my-5 py-4 text-center">
					<div class="card-body">
						<p class="text-white m-0">Kiến thức thú vị</p>
					</div>
				</div>

				<!-- Content Row -->
				<div class="row">
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">${news[0].title}</h2>
								<p class="card-text" style="background-color: red;">${news[0].content}</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">Card Two</h2>
								<p class="card-text" style="background-color: yellow;">Lorem
									ipsum dolor sit amet, consectetur adipisicing elit. Quod
									tenetur ex natus at dolorem enim! Nesciunt pariatur voluptatem
									sunt quam eaque, vel, non in id dolore voluptates quos eligendi
									labore.</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">Card Three</h2>
								<p class="card-text" style="background-color: blue;">Lorem
									ipsum dolor sit amet, consectetur adipisicing elit. Rem magni
									quas ex numquam, maxime minus quam molestias corporis quod, ea
									minima accusamus.</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->

				</div>
				<!-- /.row -->

			</div>
			<!-- /.container -->

		</div>
	</div>

</body>
</html>