<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>

	<div class="row">
		<div class="col-lg-12 col-md-10 mx-auto">
			<div class="post-preview">
				<a href="#">
					<h5 class="post-title">- Để trở thành một lập trình viên giỏi</h5>
					<h3 class="post-subtitle">
						Lập trình viên nào cũng có thể code cho máy tính hiểu.</br> Nhưng lập
						trình viên giỏi là người viết code cho cả con người hiểu
					</h3>
				</a>
				<p class="post-meta" align="right">
					<!-- Posted by <a href="#">Start Bootstrap</a> -->
					- Martin Fowler
				</p>
			</div>
			<hr>
			<!-- Page Content -->
			<div class="container">

				<!-- Call to Action Well -->
				<div class="card text-white bg-secondary my-5 py-4 text-center">
					<div class="card-body">
						<p class="text-white m-0">This call to action card is a great
							place to showcase some important information or display a clever
							tagline!</p>
					</div>
				</div>

				<div class="row">
					<!-- Content Row -->
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title"><%-- ${news[0].title} --%></h2>
								<p class="card-text" style="background-color: red;"><%-- ${news[0].content} --%></p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title"><%-- ${news[1].title} --%></h2>
								<p class="card-text" style="background-color: yellow;">Lorem
									ipsum dolor sit amet, consectetur adipisicing elit. Quod
									tenetur ex natus at dolorem enim! Nesciunt pariatur voluptatem
									sunt quam eaque, vel, non in id dolore voluptates quos eligendi
									labore.</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title"><%-- ${news[2].title} --%></h2>
								<p class="card-text" style="background-color: blue;"><%-- ${news[2].content} --%></p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
								<span style="float: right" ><%-- ${news[2].views} --%>
									<img alt="hinh anh" src="<c:url value = '/template/web/img/eye.jpg' />" width="30px" height="30px" align="right">
								</span>
							</div>
						</div>
					</div>
					<!-- /.col-md-4 -->

				</div>
				<!-- /.row -->
				

			</div>
			<!-- /.container -->

		</div>
	</div>

</body>
</html>