<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>About Me</title>

<!-- Bootstrap core CSS -->
<link
	href="<c:url value='/template/web/vendor/bootstrap/css/bootstrap.min.css' /> "
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link
	href="<c:url value='/template/web/vendor/fontawesome-free/css/all.min.css' /> "
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"
	rel='stylesheet' type='text/css'>
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
	rel='stylesheet' type='text/css'>

<!-- Custom styles for this template -->
<link
	href="<c:url value='/template/web/css/clean-blog.min.css"' />
	rel="stylesheet">

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top"
		id="mainNav">
		<div class="container">
			<a class="navbar-brand" href="index.html">Oaiker It Blogger</a>
			<button class="navbar-toggler navbar-toggler-right" type="button"
				data-toggle="collapse" data-target="#navbarResponsive"
				aria-controls="navbarResponsive" aria-expanded="false"
				aria-label="Toggle navigation">
				Menu <i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<c:forEach var="item" items="${categorys}">
						<li class="nav-item"><a class="nav-link"
							href="<c:url value="/${item.code}"/> ">${item.name} </a></li>
					</c:forEach>
					<c:if test="${not empty userModelSession}">
						<li class="nav-item"><a class="nav-link" href="#">Well
								come , ${userModelSession.fullName}</a></li>
						<li class="nav-item"><a class="nav-link"
							href="<c:url value="/oaklogout?action=logout"/> ">Thoát </a></li>
					</c:if>
 					<c:if test="${empty userModelSession}">
						<li class="nav-item"><a class="nav-link"
							href="<c:url value="/oaklogin?action=login"/> ">Đăng nhập </a></li>
					</c:if> 
				</ul>
			</div>
		</div>
	</nav>

	<!-- Page Header -->
	<header class="masthead"
		style="background-image: url('<c:url value = '/template/web/img/gach-phong-thuy-do-dam-hgcstone.jpg' />')">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-10 mx-auto">
					<div class="post-heading">
						<h2>I'M Nguyễn Bá Oai</h2>
						<span class="meta"> Tôi là 1 lập trình viên it. Sản phẩm
							được tạo ra vì những lợi ích của việc viết blog đối với bản thân,
							ngoài ra tôi mong rằng các kiến thức của bản thân có thể giúp ích
							được một số bạn trẻ sẽ hoặc đang theo ngành it, dựa vào chia sẻ
							hiểu biết của cá nhân tôi sẽ ít nhiều có một cái nhìn đa chiều
							hơn của ngành it. Những kiến thức, kinh nghiệm được chia sẻ ở đây
							sẽ có thể không truyền đạt được hết ý hiểu hoặc có thể có sai sót
							trong quá trình tìm hiểu của cá nhân. Mong được sự góp ý, trao
							đổi của mọi người để cùng nhau phát triển được bản thân</span>
						<p style="border: 5px solid white"></p>
						<span class="meta">Liên hệ :</span>
						<ul>
							<li><a href="https://www.facebook.com/oai.nguyenba.3"
								style="color: white">Facebook</a></li>
							<li><a href="oaiker2k@gmail.com" style="color: white">Gmail</a></li>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Bootstrap core JavaScript -->
	<script
		src="<c:url value='/template/web/vendor/jquery/jquery.min.js' /> "></script>
	<script
		src="<c:url value='/template/web/vendor/bootstrap/js/bootstrap.bundle.min.js' /> "></script>

	<!-- Custom scripts for this template -->
	<script src="<c:url value='/template/web/js/clean-blog.min.js'/> "></script>

</body>

</html>
