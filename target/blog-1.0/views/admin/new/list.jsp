<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách bài viết</title>
</head>

<body>
		<div class="main-content">
		<form action="<c:url value = '/admin-new' />" id="formSubmit"
			method="get">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
								chủ</a></li>
					</ul>
					<!-- /.breadcrumb -->
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Id</th>
													<th>Tiêu đề</th>
													<th>Hình ảnh</th>
													<th>Mô tả ngắn</th>
													<th>Nội dung</th>
													<th>Thể loại</th>
													<th>views</th>
													<th>likes</th>
													<th>Ngày tạo</th>
													<th>Người tạo</th>
													<th>Ngày thay đổi</th>
													<th>Người thay đổi</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="item" items="${model.listResult}">
													<tr>
														<td>${item.id}</td>
														<td>${item.title}</td>
														<td>${item.thumbnail}</td>
														<td>${item.shortDescription}</td>
														<td>${item.content}</td>
														<td>${item.categoryId}</td>
														<td>${item.views}</td>
														<td>${item.like}</td>
														<td>${item.createdDate}</td>
														<td>${item.createdBy}</td>
														<td>${item.modifiedDate}</td>
														<td>${item.modifiedBy}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<ul class="pagination" id="pagination"></ul>
										<input type="hidden" value="" id="page" name="page" /> <input
											type="hidden" value="" id="maxPageItem" name="maxPageItem" />
											<input type="hidden" value="" id="sortName" name="sortName" />
											<input type="hidden" value="" id="sortBy" name="sortBy" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script>
		// khi server gửi lên view -> sẽ lấy được page, tổng số page, số item / 1 page.
		var currentPage = ${model.page};
		var totalPages = ${model.totalPage};
		var limit = 2;// số bài viết trong 1 page.
		$(function() {
			window.pagObj = $('#pagination').twbsPagination({
				totalPages : totalPages,// tổng số page
				visiblePages : 7,// số item trong thanh Page.
				startPage : currentPage,// số trang bắt đầu đứng khi hiển thị trang
				onPageClick : function(event, page) {// giá trị tham số page này sẽ thay đổi khi ta click qua các item
					//console.info(page + ' (from options)');
					// tránh trường hợp gọi load lại page liên tục -> check nếu đang đứng thì ko load nữa.
					if (currentPage != page) {
						$('#maxPageItem').val(limit);
						$('#page').val(page);// phần input có id là page sẽ nhận được giá trị của page khi ta bấm chuyển trang.
						$('#sortName').val("id");
						$('#sortBy').val("DESC");
						$('#formSubmit').submit();// gọi submit -> gửi lên server bằng form
					}
				}
			}).on('page', function(event, page) {
				console.info(page + ' (from event listening)');
			});
		});
	</script>

</body>
</html>
