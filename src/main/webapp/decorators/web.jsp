<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="">
<title><dec:title default="blogit.com" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<c:url value='/template/web/css/bootstrap.css' />"
	rel="stylesheet">
<link
	href='http://fonts.googleapis.com/css?family=Belgrano|Courgette&subset=latin,latin-ext'
	rel='stylesheet' type='text/css'>
<link href="<c:url value='/template/web/css/bootshape.css' />"
	rel="stylesheet">
</head>

<body>
	<!-- header -->
	<%@ include file="/common/web/header.jsp"%>
	<!-- header -->

	<dec:body />

	<!-- footer -->
	<%@ include file="/common/web/footer.jsp"%>
	<!-- footer -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<c:url value='/template/web/js/jquery.js' />"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value='/template/web/js/bootstrap.min.js' />"></script>
	<script src="<c:url value='/template/web/js/bootshape.js' />"></script>
</body>

</html>
