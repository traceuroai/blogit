<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>

	<!-- Content -->
	<div class="container">
		<div class="">
			<h3 class="">Welcome</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and
				typesetting industry. Lorem Ipsum has been the industry's standard
				dummy text ever since the 1500s, when an unknown printer took a
				galley of type and scrambled it to make a type specimen book. It has
				survived not only five centuries. Lorem Ipsum is simply dummy text
				of the printing and typesetting industry. Lorem Ipsum has been the
				industry's standard dummy text ever since the 1500s, when an unknown
				printer took a galley of type and scrambled it to make a type
				specimen book. It has survived not only five centuries.</p>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<h3 class="">About</h3>
				<img src="<c:url value = '/template/web/img/about.jpg' />" alt=""
					class="img-responsive"> <br>
				<p>Lorem Ipsum is simply dummy text of the printing and
					typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s, when an unknown printer took a
					galley of type and scrambled it to make a type specimen book. It
					has survived not only five centuries. Lorem Ipsum is simply dummy
					text of.</p>
			</div>
			<div class="col-sm-4">
				<h3 class="">News & Events</h3>
				<div class="event">
					<div class="text-right date">01/22/2014</div>
					<p>Lorem Ipsum is simply dummy text of the printing and
						typesetting industr y. Lorem Ipsum has been the industry's
						standard dummy text ever since the 1500.</p>
					<div class="text-right">
						<a href="#">See more...</a>
					</div>
				</div>
				<div class="event">
					<div class="text-right date">01/22/2014</div>
					<p>Lorem Ipsum is simply dummy text of the printing and
						typesetting industr y. Lorem Ipsum has been the industry's
						standard dummy text ever since the 1500s.</p>
					<div class="text-right">
						<a href="#">See more...</a>
					</div>
				</div>
				<div class="event">
					<div class="text-right date">01/22/2014</div>
					<p>Lorem Ipsum is simply dummy text of the printing and
						typesetting industr y. Lorem Ipsum has been the industry's
						standard dummy text ever since the 1500s.</p>
					<div class="text-right">
						<a href="#">See more...</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content -->

	<!-- Thumbnails -->
	<div class="container thumbs">
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<img src="<c:url value = '/template/web/img/lay_hoa_che_mat.jpg' />"
					alt="" class="img-responsive"
					style="max-height: 200px;">
				<div class="caption">
					<h3 class="">Motor</h3>
					<p>Lorem Ipsum has been the industry's standard dummy text ever
						since the 1500s.</p>
					<div class="btn-toolbar text-center">
						<a href="#" role="button" class="btn btn-primary pull-right">Details</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<img src="<c:url value = '/template/web/img/pic2.jpg' />" alt=""
					class="img-responsive"
					style="max-height: 200px;">
				<div class="caption">
					<h3 class="">Luxury</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and
						typesetting industry. Lorem Ipsum has been the industry's standard
						dummy text ever since the 1500s.</p>
					<div class="btn-toolbar text-center">
						<a href="#" role="button" class="btn btn-primary pull-right">Details</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<img src="<c:url value = '/template/web/img/backg.jpg' />" alt=""
					class="img-responsive"
					style="max-height: 200px;">
				<div class="caption">
					<h3 class="">Sailboats</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and
						typesetting industry. Lorem Ipsum has been the industry's standard
						dummy text ever since the 1500s Lorem Ipsum is simply dummy text
						of the printing and typesetting industry. Lorem Ipsum has been the
						industry's standard dummy tex Lorem Ipsum is simply dummy text of
						the printing and typesetting industry. Lorem Ipsum has been the
						industry's standard dummy text ever since the 1500st ever since
						the 1500s</p>
					<div class="btn-toolbar text-center">
						<a href="#" role="button" class="btn btn-primary pull-right">Chi
							tiết</a>
					</div>
				</div>
			</div>

		</div>


		<div class="row">
			<c:forEach var="item" items="${model}">

				<div class="col-sm-6 col-md-4">
					<div class="thumbnail">
						<c:url var="newurl" value="/news">
							<c:param name="id" value="${item.id}" />
							<c:param name="categoryId" value="${item.categoryId}" />
						</c:url>
						<img src="<c:url value = '/template/web/img/carousel1.jpg' />"
							alt="" class="img-responsive"
							style=" max-height: 200px;">
						<div class="caption">
							<h4 class="">
								<a href="${newurl}" style="color: black;">${item.title}</a>
							</h4>
							<span>
								<span style="text-align: left; font-size: 16px;"><i>${item.createdDate}</i></span>
								<span style="float: right;">${item.views}
								<i class="fas fa-eye"></i></span>
							</span>
							<br>
							<div class="btn-toolbar text-center">
								<a href="#" role="button" class="btn btn-primary pull-right">Chi
									tiết </a>
							</div>
						</div>
					</div>
				</div>
				<br>
				<br>
			</c:forEach>
		</div>
	</div>
	<!-- End Thumbnails -->

</body>
</html>