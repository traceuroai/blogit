<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE >
<html>

<head>
<title>About Me</title>
</head>

<body>

	<div>
		<h2>Xin chào các bạn</h2>
		<span class="meta">I'm Oai </br> </br> Là 1 lập trình viên it tôi xây
			dựng blog một phần vì những lợi ích của việc viết blog đối với bản
			thân, ngoài ra tôi mong rằng các kiến thức của bản thân có thể giúp
			ích được một số bạn trẻ sẽ hoặc đang theo ngành it, dựa vào chia sẻ
			hiểu biết của cá nhân tôi sẽ ít nhiều có một cái nhìn đa chiều hơn
			của ngành it. </br> </br> Những kiến thức, kinh nghiệm được chia sẻ ở đây sẽ có
			thể không truyền đạt được hết ý hiểu hoặc có thể có sai sót trong quá
			trình tìm hiểu của cá nhân. </br> </br> Tôi luôn mong được sự góp ý, giúp đỡ
			của mọi người để những bài viết ngày càng chất lượng hơn và cùng nhau
			phát triển.
		</span> </br>
		<p style="border: 5px solid white"></p>
		<span class="meta"><i>Mọi ý kiến góp ý xin vui lòng liên hệ
				:</i></span> </br>
		<ul>
			<li><a href="https://www.facebook.com/oai.nguyenba.3"
				style="color: blue">Facebook</a></li>
			<li><a href="oaiker2k@gmail.com" style="color: blue">Gmail</a></li>
		</ul>
		<h4>Cám ơn các bạn đã ghé thăm !</h4>
	</div>

</body>

</html>

