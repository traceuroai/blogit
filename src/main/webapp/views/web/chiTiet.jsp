<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<div class="row">
		<%-- <div class="col-lg-12 col-md-10 mx-auto">
			<hr>
			<!-- Page Content -->
			<div class="container">
				
				<h2 class="card-title">${news.title}</h2>
				
				<p>${news.content}</p>
				
				
				<!-- Content Row -->
				<div class="row">
					<div class="col-md-4 mb-5">
						<div class="card h-100">
							<div class="card-body">
								<h2 class="card-title">${news.title}</h2>
								<p class="card-text" style="background-color: red;">${news[0].content}</p>
							</div>
							<div class="card-footer">
								<a href="#" class="btn btn-primary btn-sm">More Info</a>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
				
			</div>
			<!-- /.container -->
		</div> --%>

		<div class="col-lg-4 col-sm-6 mb-4">
			<div class="portfolio-item">
				<a class="portfolio-link" data-toggle="modal"
					href="#portfolioModal1">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fas fa-plus fa-3x"></i>
						</div>
					</div> <img class="img-fluid" src="assets/img/portfolio/01-thumbnail.jpg"
					alt="" />
				</a>
				<div class="portfolio-caption">
					<div class="portfolio-caption-heading">Threads</div>
					<div class="portfolio-caption-subheading text-muted">Illustration</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>