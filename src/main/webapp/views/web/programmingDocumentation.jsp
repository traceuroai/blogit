<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>

	<div class="row">
		<div class="col-lg-12 col-md-10 mx-auto">
			<hr>
			<!-- Page Content -->
			<div class="container">

				<!-- Call to Action Well -->
				<div class="card text-white bg-secondary my-5 py-4 text-center">
					<div class="card-body">
						<p class="text-white m-0">Tài liệu lập trình</p>
					</div>
				</div>

				<!-- Content Row -->
				<div class="row">

					<c:forEach var="item" items="${news}">
						<div class="col-md-4 mb-5">
							<div class="card h-70">
								<div class="card-body"
									style="background-image : url('<c:url value = '/template/web/img/backg.jpg' />')">
									<br> <br> <br> <br> <br> <br>
								</div>
							</div>
							<div class="card h-30">
								<c:url var="newurl" value="/news">
									<c:param name="id" value="${item.id}" />
									<c:param name="categoryId" value="${item.categoryId}" />
								</c:url>
								<div>
									<a href="${newurl}"><b>${item.title}</b></a>
								</div>
								<div class="card-footer"></div>
							</div>
						</div>
						<!-- /.col-md-4 -->
					</c:forEach>

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</div>
	</div>

</body>
</html>