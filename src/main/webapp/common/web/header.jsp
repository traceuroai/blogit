<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>

<!-- Navigation bar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<c:url value="/home"/>">Oaiker It
				Blogger</a>
		</div>
		<nav role="navigation" class="collapse navbar-collapse navbar-right">
			<ul class="navbar-nav nav">

				<c:forEach var="item" items="${categorys}">
					<li class="active"><a href="<c:url value="/${item.code}"/>">${item.name}</a></li>
				</c:forEach>
				<li class="dropdown"><a data-toggle="dropdown" href="#"
					class="dropdown-toggle">Khác <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="active"><a href="#">Item 1</a></li>
						<li><a href="#">Item 2</a></li>
						<li><a href="#">Item 3</a></li>
						<li class="divider"></li>
						<li><a href="#">All Items</a></li>
					</ul></li>

				<c:if test="${not empty userModelSession}">
					<li class="nav-item"><a class="nav-link" href="#">Well
							come , ${userModelSession.fullName}</a></li>
					<li class="nav-item"><a class="nav-link"
						href="<c:url value="/oaklogout?action=logout"/> ">Thoát </a></li>
				</c:if>
				<%-- <c:if test="${empty userModelSession}">
					<li class="nav-item"><a class="nav-link"
						href="<c:url value="/oaklogin?action=login"/> ">Đăng nhập </a></li>
				</c:if> --%>
			</ul>
		</nav>
	</div>
</div>
<!-- End Navigation bar -->

<!-- Slide gallery -->
<div class="jumbotron">
	<div class="container">
		<div class="col-xs-12">
			<div id="carousel-example-generic" class="carousel slide"
				data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0"
						class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img src="<c:url value="/template/web/img/carousel1.jpg"/>" alt="">
						<div class="carousel-caption"></div>
					</div>
					<div class="item">
						<img src="<c:url value="/template/web/img/carousel2.jpg"/>" alt="">
						<div class="carousel-caption"></div>
					</div>
					<div class="item">
						<img src="<c:url value="/template/web/img/carousel3.jpg"/>" alt="">
						<div class="carousel-caption"></div>
					</div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left"></span>
				</a> <a class="right carousel-control" href="#carousel-example-generic"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
	</div>
	<!-- End Slide gallery -->
</div>