package com.oaikerblogit.mapper.implement;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.oaikerblogit.mapper.RowMapper;
import com.oaikerblogit.model.CategoryModel;

public class CategoryMapper implements RowMapper<CategoryModel>{

	@Override
	public CategoryModel Maprow(ResultSet resultSet) {
		try {
			CategoryModel category = new CategoryModel();
			category.setId(resultSet.getLong("id"));
			category.setCode(resultSet.getString("code"));
			category.setName(resultSet.getString("name"));
			category.setCreatedDate(resultSet.getTimestamp("createddate"));
			category.setCreatedBy(resultSet.getString("createdby"));
			category.setModifiedDate(resultSet.getTimestamp("modifieddate"));
			category.setModifiedBy(resultSet.getString("modifiedby"));
			return category;
		} catch (SQLException e) {
			return null;
		}
	}
}
