package com.oaikerblogit.mapper.implement;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.oaikerblogit.mapper.RowMapper;
import com.oaikerblogit.model.NewModel;

public class NewMapper implements RowMapper<NewModel> {

	// việc select dữ liệu lên sẽ trả về 1 đối tượng resultset. đối tượng resultset
	// này đưuọc lưu dưới dạng bảng build method để map hết dl của resultset vào
	// model tương ứng
	@Override
	public NewModel Maprow(ResultSet resultSet) {
		try {
			NewModel newModel = new NewModel();
			newModel.setId(resultSet.getLong("id"));
			newModel.setTitle(resultSet.getString("title"));
			newModel.setThumbnail(resultSet.getString("thumbnail"));
			newModel.setShortDescription(resultSet.getString("shortdescription"));
			newModel.setContent(resultSet.getString("content"));
			newModel.setCategoryId(resultSet.getLong("categoryid"));
			newModel.setLike(resultSet.getInt("likes"));
			newModel.setViews(resultSet.getInt("views"));
			newModel.setCreatedDate(resultSet.getTimestamp("createddate"));
			newModel.setCreatedBy(resultSet.getString("createdby"));
//			newModel.setModifiedDate(resultSet.getTimestamp("modifieddate"));
//			newModel.setModifiedBy(resultSet.getString("modifiedby"));
			if (resultSet.getTimestamp("modifieddate") != null) {
				newModel.setModifiedDate(resultSet.getTimestamp("modifieddate"));
			}
			if (resultSet.getString("modifiedby") != null) {
				newModel.setModifiedBy(resultSet.getString("modifiedby"));
			}
			return newModel;
		} catch (SQLException e) {
			return null;
		}
	}
}
