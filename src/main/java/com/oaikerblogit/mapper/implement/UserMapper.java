package com.oaikerblogit.mapper.implement;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.oaikerblogit.mapper.RowMapper;
import com.oaikerblogit.model.RoleModel;
import com.oaikerblogit.model.UserModel;

public class UserMapper implements RowMapper<UserModel> {

	@Override
	public UserModel Maprow(ResultSet resultSet) {
		try {
			UserModel user = new UserModel();
			user.setId(resultSet.getLong("id"));
			user.setUserName(resultSet.getString("username"));
			user.setPassword(resultSet.getString("password"));
			user.setFullName(resultSet.getString("fullname"));
			user.setStatus(resultSet.getInt("status"));
			// trường hợp câu query không có innerjoin -> name,code sẽ lỗi -> bắt ex
			try {
				RoleModel role = new RoleModel();
				role.setCode(resultSet.getString("code"));
				role.setName(resultSet.getString("name"));
				user.setRole(role);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return user;
		} catch (SQLException e) {
			return null;
		}
	}

}
