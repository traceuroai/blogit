package com.oaikerblogit.mapper;

import java.sql.ResultSet;

public interface RowMapper<T> {

	T Maprow(ResultSet resultSet);
}
