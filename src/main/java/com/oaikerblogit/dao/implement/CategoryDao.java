package com.oaikerblogit.dao.implement;

import java.util.List;

import com.oaikerblogit.dao.ICategoryDao;
import com.oaikerblogit.mapper.implement.CategoryMapper;
import com.oaikerblogit.model.CategoryModel;

public class CategoryDao extends AbstractDao<CategoryModel> implements ICategoryDao {

	@Override
	public List<CategoryModel> findAll() {
		String sql = "SELECT * FROM category";
		return query(sql, new CategoryMapper());
	}

	@Override
	public CategoryModel findOneById(Long id) {
		String sql = "SELECT * FROM category where id = ?";
		List<CategoryModel> category = query(sql, new CategoryMapper(), id);// query luôn trả về 1 list
		return category.isEmpty() ? null : category.get(0);
	}

	@Override
	public CategoryModel findOneByCode(String code) {
		String sql = "SELECT * FROM category where code = ?";
		List<CategoryModel> category = query(sql, new CategoryMapper(), code);
		return category.isEmpty() ? null : category.get(0);
	}

	@Override
	public Long insert(CategoryModel categoryModel) {
		StringBuilder sql = new StringBuilder(
				"INSERT INTO category (name, code, createddate, modifieddate, createdby, modifiedby ) ");
		sql.append(" VALUES ( ?, ?, ?, ?, ?, ? )");
		Long id = insert(sql.toString(), categoryModel.getName(), categoryModel.getCode(),
				categoryModel.getCreatedDate(), categoryModel.getModifiedDate(), categoryModel.getCreatedBy(),
				categoryModel.getModifiedBy());
		return id;
	}

	@Override
	public Long update(CategoryModel categoryModel) {
		StringBuilder sql = new StringBuilder(
				"UPDATE category SET name = ?, code = ?, createddate = ?, modifieddate = ?, createdby = ?, modifiedby = ? WHERE id = ?");
		Long id = update(sql.toString(), categoryModel.getName(), categoryModel.getCode(),
				categoryModel.getCreatedDate(), categoryModel.getModifiedDate(), categoryModel.getCreatedBy(),
				categoryModel.getModifiedBy(), categoryModel.getId());
		return id;
	}

	@Override
	public String delete(Long Id) {
		String sql = "DELETE FROM category WHERE id = ?";
		return delete(sql, Id);
	}
}
