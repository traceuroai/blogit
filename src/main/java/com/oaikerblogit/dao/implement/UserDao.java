package com.oaikerblogit.dao.implement;

import java.util.List;

import com.oaikerblogit.dao.IUserDao;
import com.oaikerblogit.mapper.implement.UserMapper;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.paging.Pageble;

public class UserDao extends AbstractDao<UserModel> implements IUserDao {

	@Override
	public UserModel findbyUserAndPasswordAndStatus(String userName, String password, Integer status) {
		StringBuilder sql = new StringBuilder("SELECT * FROM user AS u ");
		sql.append(" INNER JOIN role AS r ON r.id = u.roleid");
		sql.append(" WHERE username = ? AND password = ? AND status = ?");
		List<UserModel> users = query(sql.toString(), new UserMapper(), userName, password, status);
		return users.isEmpty() ? null : users.get(0);
	}

	@Override
	public List<UserModel> findAll(Pageble pageble) {
		// trường hợp không phân trang :
		StringBuilder sql = new StringBuilder("SELECT * FROM user ");
		if (pageble.getSorter() != null) {
			sql.append(" ORDER BY " + pageble.getSorter().getSortName() + " " + pageble.getSorter().getSortBy() + " ");
		}
		if (pageble.getOffset() != null && pageble.getLimit() != null) {
			sql.append("LIMIT " + pageble.getOffset() + ", " + pageble.getLimit());
//					return query(sql.toString(), new NewMapper(), offset, limit);
		}
		return query(sql.toString(), new UserMapper());
	}

	@Override
	public UserModel findOneById(Long id) {
		String sql = "SELECT * FROM user where id = ? ";
		List<UserModel> users = query(sql, new UserMapper(), id);
		return users.isEmpty() ? null : users.get(0);
	}

	@Override
	public UserModel findOneByUserName(String userName) {
		String sql = "SELECT * FROM user where username = ? ";
		List<UserModel> users = query(sql, new UserMapper(), userName);
		return users.isEmpty() ? null : users.get(0);
	}

	@Override
	public Long insert(UserModel userModel) {
		StringBuilder sql = new StringBuilder(
				"INSERT INTO user ( username, password, fullname, status, roleid, createddate, modifieddate, createdby, modifiedby ) ");
		sql.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
		return insert(sql.toString(), userModel.getUserName(), userModel.getPassword(), userModel.getFullName(),
				userModel.getStatus(), userModel.getRoleId(), userModel.getCreatedDate(), userModel.getModifiedDate(),
				userModel.getCreatedBy(), userModel.getModifiedBy());
	}

	@Override
	public Long update(UserModel userModel) {
		StringBuilder sql = new StringBuilder("UPDATE user SET username = ?, password = ?, fullname = ?, ");
		sql.append(
				" status = ?, roleid = ?, createddate = ?, createdby = ?, modifieddate = ?, modifiedby = ? WHERE id = ?");
		return insert(sql.toString(), userModel.getUserName(), userModel.getPassword(), userModel.getFullName(),
				userModel.getStatus(), userModel.getRoleId(), userModel.getCreatedDate(), userModel.getCreatedBy(),
				userModel.getModifiedDate(), userModel.getModifiedBy(), userModel.getId());
	}

	@Override
	public String delete(Long Id) {
		String sql = "DELETE FROM user WHERE id = ?";
		return delete(sql, Id);
	}

	@Override
	public int getTotalItem() {
		String sql = "SELECT count(*) FROM user ";
		return count(sql);
	}

	@Override
	public String deleteByRoleId(Long Id) {
		String sql = "DELETE FROM user WHERE roleid = ?";
		return delete(sql, Id);
	}

	@Override
	public List<UserModel> findByRoleId(Long roleId) {
		String sql = "SELECT * FROM user where roleid = ? ";
		return query(sql, new UserMapper(), roleId);
	}

}
