package com.oaikerblogit.dao.implement;

import java.util.List;

import com.oaikerblogit.dao.IRoleDao;
import com.oaikerblogit.mapper.implement.RoleMapper;
import com.oaikerblogit.model.RoleModel;

public class RoleDao extends AbstractDao<RoleModel> implements IRoleDao{

	@Override
	public List<RoleModel> findAll() {
		String sql = "SELECT * FROM role";
		return query(sql, new RoleMapper());
	}

	@Override
	public RoleModel findOneById(Long id) {
		String sql = "SELECT * FROM role where id = ?";
		List<RoleModel> roleModel = query(sql, new RoleMapper(), id);// query luôn trả về 1 list
		return roleModel.isEmpty() ? null : roleModel.get(0);
	}

	@Override
	public RoleModel findOneByCode(String code) {
		String sql = "SELECT * FROM role where code = ?";
		List<RoleModel> roleModel = query(sql, new RoleMapper(), code);
		return roleModel.isEmpty() ? null : roleModel.get(0);
	}

	@Override
	public Long insert(RoleModel roleModel) {
		StringBuilder sql = new StringBuilder(
				"INSERT INTO role (name, code, createddate, modifieddate, createdby, modifiedby ) ");
		sql.append(" VALUES ( ?, ?, ?, ?, ?, ? )");
		Long id = insert(sql.toString(), roleModel.getName(), roleModel.getCode(),
				roleModel.getCreatedDate(), roleModel.getModifiedDate(), roleModel.getCreatedBy(),
				roleModel.getModifiedBy());
		return id;
	}

	@Override
	public Long update(RoleModel roleModel) {
		StringBuilder sql = new StringBuilder(
				"UPDATE role SET name = ?, code = ?, createddate = ?, modifieddate = ?, createdby = ?, modifiedby = ? WHERE id = ?");
		Long id = update(sql.toString(), roleModel.getName(), roleModel.getCode(),
				roleModel.getCreatedDate(), roleModel.getModifiedDate(), roleModel.getCreatedBy(),
				roleModel.getModifiedBy(), roleModel.getId());
		return id;
	}

	@Override
	public String delete(Long Id) {
		String sql = "DELETE FROM role WHERE id = ?";
		return delete(sql, Id);
	}

}
