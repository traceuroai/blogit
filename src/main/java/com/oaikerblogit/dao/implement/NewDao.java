package com.oaikerblogit.dao.implement;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.oaikerblogit.dao.INewDao;
import com.oaikerblogit.mapper.implement.NewMapper;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.paging.Pageble;

public class NewDao extends AbstractDao<NewModel> implements INewDao {

	@Override
	public List<NewModel> findAll(Pageble pageble) {
		// trường hợp không phân trang :
		StringBuilder sql = new StringBuilder("SELECT * FROM news ");
		if (pageble.getSorter() != null && StringUtils.isNotBlank(pageble.getSorter().getSortName())
				&& StringUtils.isNotBlank(pageble.getSorter().getSortBy())) {// isNotBlank : thư viện commonlang 2.6 cung cấp (kiểm tra rỗng, null)
			sql.append(" ORDER BY " + pageble.getSorter().getSortName() + " " + pageble.getSorter().getSortBy() + " ");
		}
		if (pageble.getOffset() != null && pageble.getLimit() != null) {
			sql.append("LIMIT " + pageble.getOffset() + ", " + pageble.getLimit());
//			return query(sql.toString(), new NewMapper(), offset, limit);
		}
		return query(sql.toString(), new NewMapper());
	}

	@Override
	public List<NewModel> findByCategoryId(Long categoryId) {
		String sql = "SELECT * FROM news where categoryid = ? ";
		return query(sql, new NewMapper(), categoryId);
	}

	@Override
	public NewModel findOneById(Long Id) {
		String sql = "SELECT * FROM news where id = ? ";
		List<NewModel> news = query(sql, new NewMapper(), Id);
		return news.isEmpty() ? null : news.get(0);

	}

	@Override
	public NewModel findOneByTitle(String title) {
		String sql = "SELECT * FROM news where title = ? ";
		List<NewModel> news = query(sql, new NewMapper(), title);
		return news.isEmpty() ? null : news.get(0);
	}

	@Override
	public Long insert(NewModel newModel) {
		StringBuilder sql = new StringBuilder(
				"INSERT INTO news ( title, thumbnail, shortdescription, content, categoryid, likes, views, createddate, modifieddate, createdby, modifiedby ) ");
		sql.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
		return insert(sql.toString(), newModel.getTitle(), newModel.getThumbnail(), newModel.getShortDescription(),
				newModel.getContent(), newModel.getCategoryId(), newModel.getLike(), newModel.getViews(),
				newModel.getCreatedDate(), newModel.getModifiedDate(), newModel.getCreatedBy(),
				newModel.getModifiedBy());
	}

	@Override
	public Long update(NewModel newModel) {
		StringBuilder sql = new StringBuilder("UPDATE news SET title = ?, thumbnail = ?, shortdescription = ?, ");
		sql.append(
				" content = ?, categoryid = ?, likes = ?, views = ?, createddate = ?, createdby = ?, modifieddate = ?, modifiedby = ? WHERE id = ?");
		return insert(sql.toString(), newModel.getTitle(), newModel.getThumbnail(), newModel.getShortDescription(),
				newModel.getContent(), newModel.getCategoryId(), newModel.getLike(), newModel.getViews(),
				newModel.getCreatedDate(), newModel.getCreatedBy(), newModel.getModifiedDate(),
				newModel.getModifiedBy(), newModel.getId());
	}

	@Override
	public String delete(Long Id) {
		String sql = "DELETE FROM news WHERE id = ?";
		return delete(sql, Id);

	}

	@Override
	public String deleteByCategoryId(Long Id) {
		String sql = "DELETE FROM news WHERE categoryid = ?";
		return delete(sql, Id);
	}

	@Override
	public int getTotalItem() {
		String sql = "SELECT count(*) FROM news ";
		return count(sql);
	}
}
