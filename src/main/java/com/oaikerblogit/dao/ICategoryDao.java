package com.oaikerblogit.dao;

import java.util.List;

import com.oaikerblogit.model.CategoryModel;

public interface ICategoryDao extends GenericDao<CategoryModel> {

	List<CategoryModel> findAll();

	CategoryModel findOneById(Long id);

	CategoryModel findOneByCode(String code);

	Long insert(CategoryModel categoryModel);

	Long update(CategoryModel categoryModel);

	String delete(Long Id);
}
