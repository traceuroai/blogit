package com.oaikerblogit.dao;

import java.util.List;

import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.paging.Pageble;

public interface IUserDao extends GenericDao<UserModel> {

	UserModel findbyUserAndPasswordAndStatus(String userName, String password, Integer status);

	List<UserModel> findAll(Pageble pageble);
	
	List<UserModel> findByRoleId(Long roleId);

	UserModel findOneById(Long id);

	UserModel findOneByUserName(String userName);

	Long insert(UserModel userModel);

	Long update(UserModel userModel);

	String delete(Long Id);
	
	String deleteByRoleId(Long Id);
	
	int getTotalItem();

}
