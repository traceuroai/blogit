package com.oaikerblogit.dao;

import java.util.List;

import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.paging.Pageble;

public interface INewDao extends GenericDao<NewModel> {

	List<NewModel> findAll(Pageble pageble);

	List<NewModel> findByCategoryId(Long categoryId);

	NewModel findOneById(Long Id);

	NewModel findOneByTitle(String title);

	Long insert(NewModel newModel);

	Long update(NewModel newModel);

	String delete(Long Id);

	String deleteByCategoryId(Long Id);

// nhiệm vụ phân trang
	int getTotalItem();
}
