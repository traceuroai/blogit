package com.oaikerblogit.dao;

import java.util.List;

import com.oaikerblogit.model.RoleModel;

public interface IRoleDao extends GenericDao<RoleModel>{
	
	List<RoleModel> findAll();

	RoleModel findOneById(Long id);

	RoleModel findOneByCode(String code);

	Long insert(RoleModel roleModel);

	Long update(RoleModel roleModel);

	String delete(Long Id);

}
