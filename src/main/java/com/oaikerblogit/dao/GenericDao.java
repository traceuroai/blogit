package com.oaikerblogit.dao;

import java.util.List;

import com.oaikerblogit.mapper.RowMapper;

public interface GenericDao<T> {

	<T> List<T> query(String sql, RowMapper<T> rowMapper, Object... parameters);
	Long update(String sql, Object... parameters );
	Long insert(String sql, Object... parameters );
	String delete(String sql, Object... parameters );
	int count(String sql, Object... parameters);
}
