package com.oaikerblogit.dao;

import com.oaikerblogit.model.CommentModel;

public interface ICommentDao extends GenericDao<CommentModel>{

}
