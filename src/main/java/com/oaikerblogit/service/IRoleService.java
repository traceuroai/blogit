package com.oaikerblogit.service;

import java.util.List;

import com.oaikerblogit.model.RoleModel;

public interface IRoleService {

	List<RoleModel> findAll();

	RoleModel findOneById(Long id);

	RoleModel findOneByCode(String code);

	RoleModel insert(RoleModel roleModel);

	RoleModel update(RoleModel roleModel);

	void delete(Long[] id);

}
