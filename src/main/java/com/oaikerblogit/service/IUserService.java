package com.oaikerblogit.service;

import java.util.List;

import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.paging.Pageble;

public interface IUserService {

	UserModel findbyUserAndPasswordAndStatus(String userName, String password, Integer status);
	
	List<UserModel> findAll(Pageble pageble);

	List<UserModel> findByRoleId(Long roleId);

	UserModel findOneById(Long Id);

	UserModel findOneByUserName(String username);

	UserModel insert(UserModel userModel);

	UserModel update(UserModel userModel);

	void delete(Long[] ids);

// nhiệm vụ phân trang
	int getTotalItem();
	
}
