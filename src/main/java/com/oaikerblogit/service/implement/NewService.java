package com.oaikerblogit.service.implement;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import com.oaikerblogit.dao.ICategoryDao;
import com.oaikerblogit.dao.INewDao;
import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.paging.Pageble;
import com.oaikerblogit.service.INewService;

public class NewService implements INewService {

	@Inject
	private INewDao newDao;

	@Inject
	private ICategoryDao categoryDao;

	@Override
	public List<NewModel> findAll(Pageble pageble) {
		return newDao.findAll(pageble);
	}

	@Override
	public List<NewModel> findByCategoryId(Long categoryId) {
		return newDao.findByCategoryId(categoryId);
	}

	@Override
	public NewModel findOneById(Long Id) {
		NewModel newModel = newDao.findOneById(Id);
		CategoryModel categoryModel = categoryDao.findOneById(newModel.getCategoryId());
		newModel.setCategoryCode(categoryModel.getCode());
		newModel.setViews(newModel.getViews() + 1);
		newDao.update(newModel);
		return newModel;
	}

	@Override
	public NewModel findOneByTitle(String title) {
		return newDao.findOneByTitle(title);
	}

	@Override
	public NewModel insert(NewModel newModel) {
		CategoryModel categoryModel = categoryDao.findOneByCode(newModel.getCategoryCode());
		if (categoryModel != null) {
			newModel.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			newModel.setCategoryId(categoryModel.getId());
			Long id = newDao.insert(newModel);
			if (id != null) {
				return newDao.findOneById(id);
			}
		}
		return null;
	}

	@Override
	public NewModel update(NewModel newModel) {
		NewModel existNew = null;
		if (newModel != null) {
			existNew = newDao.findOneById(newModel.getId());
			if (existNew != null) {
				CategoryModel category = categoryDao.findOneByCode(newModel.getCategoryCode());
				if (category != null) {
					existNew.setId(newModel.getId());
					existNew.setTitle(newModel.getTitle());
					existNew.setThumbnail(newModel.getThumbnail());
					existNew.setShortDescription(newModel.getShortDescription());
					existNew.setContent(newModel.getContent());
					existNew.setCategoryId(category.getId());
					existNew.setLike(newModel.getLike());
					existNew.setViews(newModel.getViews());
					existNew.setCreatedDate(existNew.getCreatedDate());
					existNew.setCreatedBy(existNew.getCreatedBy());
					existNew.setModifiedDate(new Timestamp(System.currentTimeMillis()));
					existNew.setModifiedBy(newModel.getModifiedBy());
					newDao.update(existNew);
					return existNew;
				}
			}
		}
		return null;
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids)
		{
			newDao.delete(id);
		}
	}

	@Override
	public int getTotalItem() {
		return newDao.getTotalItem();
	}

}
