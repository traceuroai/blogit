package com.oaikerblogit.service.implement;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import com.oaikerblogit.dao.IRoleDao;
import com.oaikerblogit.dao.IUserDao;
import com.oaikerblogit.model.RoleModel;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.paging.Pageble;
import com.oaikerblogit.service.IUserService;

public class UserService implements IUserService{
	
	@Inject
	private IUserDao userDao;
	
	@Inject
	private IRoleDao roleDao;

	@Override
	public UserModel findbyUserAndPasswordAndStatus(String userName, String password, Integer status) {
		if(userName != null && password != null && status != null)
		{
			return userDao.findbyUserAndPasswordAndStatus(userName, password, status);
		}
		return null;
	}

	@Override
	public List<UserModel> findAll(Pageble pageble) {
		return userDao.findAll(pageble);
	}

	@Override
	public List<UserModel> findByRoleId(Long roleId) {
		return userDao.findByRoleId(roleId);
	}

	@Override
	public UserModel findOneById(Long Id) {
		UserModel userModel = userDao.findOneById(Id);
		RoleModel roleModel = roleDao.findOneById(userModel.getRoleId());
		userModel.setRole(roleModel);
		return userModel;
	}

	@Override
	public UserModel findOneByUserName(String username) {
		return userDao.findOneByUserName(username);
	}

	@Override
	public UserModel insert(UserModel userModel) {
		RoleModel RoleModel = roleDao.findOneByCode(userModel.getRole().getCode());
		if (RoleModel != null) {
			userModel.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			userModel.setRoleId(RoleModel.getId());
			Long id = userDao.insert(userModel);
			if (id != null) {
				return userDao.findOneById(id);
			}
		}
		return null;
	}

	@Override
	public UserModel update(UserModel userModel) {
		UserModel existUser = null;
		if (userModel != null) {
			existUser = userDao.findOneById(userModel.getId());
			if (existUser != null) {
				RoleModel role = roleDao.findOneByCode(userModel.getRole().getCode());
				if (role != null) {
					existUser.setId(userModel.getId());
					existUser.setUserName(userModel.getUserName());
					existUser.setPassword(userModel.getPassword());
					existUser.setFullName(userModel.getFullName());
					existUser.setStatus(userModel.getStatus());
					existUser.setRoleId(role.getId());
					existUser.setCreatedDate(existUser.getCreatedDate());
					existUser.setCreatedBy(existUser.getCreatedBy());
					existUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
					existUser.setModifiedBy(userModel.getModifiedBy());
					userDao.update(existUser);
					return existUser;
				}
			}
		}
		return null;
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids)
		{
			userDao.delete(id);
		}
	}

	@Override
	public int getTotalItem() {
		return userDao.getTotalItem();
	}

}
