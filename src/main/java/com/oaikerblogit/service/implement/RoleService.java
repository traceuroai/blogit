package com.oaikerblogit.service.implement;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import com.oaikerblogit.dao.IRoleDao;
import com.oaikerblogit.dao.IUserDao;
import com.oaikerblogit.model.RoleModel;
import com.oaikerblogit.service.IRoleService;

public class RoleService implements IRoleService {

	@Inject
	private IRoleDao roleDao;

	@Inject
	private IUserDao userDao;

	@Override
	public List<RoleModel> findAll() {
		return roleDao.findAll();
	}

	@Override
	public RoleModel findOneById(Long id) {
		return roleDao.findOneById(id);
	}

	@Override
	public RoleModel findOneByCode(String code) {
		return roleDao.findOneByCode(code);
	}

	@Override
	public RoleModel insert(RoleModel roleModel) {
		if (roleModel != null) {
			if ((roleModel.getCode() != null && roleModel.getName() != null)
					&& (!roleModel.getCode().isEmpty() && !roleModel.getName().isEmpty())) {
				if (roleDao.findOneByCode(roleModel.getCode()) == null)// chưa tồn tại
				{
					roleModel.setCreatedDate(new Timestamp(System.currentTimeMillis()));
					Long id = roleDao.insert(roleModel);
					if (id != null) {
						return roleDao.findOneById(id);
					}
				}
			}
		}
		return null;
	}

	@Override
	public RoleModel update(RoleModel roleModel) {
		if (roleModel != null) {
			if ((roleModel.getCode() != null && roleModel.getName() != null)
					&& (!roleModel.getCode().isEmpty() && !roleModel.getName().isEmpty())) {
				// lấy dl cũ lên
				RoleModel existRole = roleDao.findOneById(roleModel.getId());
				if (existRole != null) {
					existRole.setCode(roleModel.getCode());
					existRole.setName(roleModel.getName());
					existRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
					existRole.setModifiedBy(roleModel.getModifiedBy());
					roleDao.update(existRole);
					return existRole;
				}
			}
		}
		return null;
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids)
		{
			userDao.deleteByRoleId(id);
			roleDao.delete(id);
		}
	}

}
