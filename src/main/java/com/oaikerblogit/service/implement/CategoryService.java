package com.oaikerblogit.service.implement;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import com.oaikerblogit.dao.ICategoryDao;
import com.oaikerblogit.dao.INewDao;
import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.service.ICategoryService;

public class CategoryService implements ICategoryService {

	// weld để sử dụng được @Inject(tương tự @Autowired)
	@Inject
	private ICategoryDao categoryDao;

	@Inject
	private INewDao newDao;

	@Override
	public List<CategoryModel> findAll() {
		return categoryDao.findAll();
	}

	@Override
	public CategoryModel insert(CategoryModel category) {
		if (category != null) {
			if ((category.getCode() != null && category.getName() != null)
					&& (!category.getCode().isEmpty() && !category.getName().isEmpty())) {
				if (categoryDao.findOneByCode(category.getCode()) == null)// chưa tồn tại
				{
					category.setCreatedDate(new Timestamp(System.currentTimeMillis()));
					Long id = categoryDao.insert(category);
					if (id != null) {
						return categoryDao.findOneById(id);
					}
				}
			}
		}
		return null;
	}

	@Override
	public CategoryModel update(CategoryModel categoryModel) {
		if (categoryModel != null) {
			if ((categoryModel.getCode() != null && categoryModel.getName() != null)
					&& (!categoryModel.getCode().isEmpty() && !categoryModel.getName().isEmpty())) {
				// lấy dl cũ lên
				CategoryModel existCategory = categoryDao.findOneById(categoryModel.getId());
				if (existCategory != null) {
					existCategory.setCode(categoryModel.getCode());
					existCategory.setName(categoryModel.getName());
					existCategory.setModifiedDate(new Timestamp(System.currentTimeMillis()));
					existCategory.setModifiedBy(categoryModel.getModifiedBy());
					categoryDao.update(existCategory);
					return existCategory;
				}
			}
		}
		return null;
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids)
		{
			newDao.deleteByCategoryId(id);
			categoryDao.delete(id);
		}
	}

	@Override
	public CategoryModel findOneById(Long id) {
		return categoryDao.findOneById(id);
	}

	@Override
	public CategoryModel findOneByCode(String code) {
		return categoryDao.findOneByCode(code);
	}

}
