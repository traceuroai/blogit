package com.oaikerblogit.service;

import java.util.List;

import com.oaikerblogit.model.CategoryModel;

public interface ICategoryService {

	List<CategoryModel> findAll();

	CategoryModel findOneById(Long id);

	CategoryModel findOneByCode(String code);

	CategoryModel insert(CategoryModel ct);

	CategoryModel update(CategoryModel categoryModel);

	void delete(Long[] id);
}
