package com.oaikerblogit.service;

import java.util.List;

import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.paging.Pageble;

public interface INewService {

	List<NewModel> findAll(Pageble pageble);

	List<NewModel> findByCategoryId(Long categoryId);

	NewModel findOneById(Long Id);

	NewModel findOneByTitle(String title);

	NewModel insert(NewModel newModel);

	NewModel update(NewModel newModel);

	void delete(Long[] ids);

// nhiệm vụ phân trang
	int getTotalItem();
}
