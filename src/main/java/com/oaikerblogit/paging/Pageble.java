package com.oaikerblogit.paging;

import com.oaikerblogit.sort.Sorter;

public interface Pageble {

	Integer getPage();
	
	Integer getOffset();
	
	Integer getLimit();
	
	Sorter getSorter();
	
}
