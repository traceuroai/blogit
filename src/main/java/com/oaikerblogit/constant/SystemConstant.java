package com.oaikerblogit.constant;

/* build các hàm field chung để giao tiếp giữa view và controller -> model
 * 
 * */

public class SystemConstant {

	public static final String MODEL = "model";
	public static final String ADMIN = "ADMIN";
	public static final String USER = "USER";
	// type thêm mới, cập nhật bài viết
	public static final String LIST = "list";
	public static final String EDIT = "edit";

}
