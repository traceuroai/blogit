package com.oaikerblogit.util;

import java.io.BufferedReader;

import com.fasterxml.jackson.databind.ObjectMapper;

/* chuyển chuỗi json thành 1 đối tượng với các key của json là các field của đối tượng đó
 * 
 * 
 * */

public class HttpUtil {

	private String value;

	public HttpUtil(String vl) {
		this.value = vl;
	}
	
	
	// mapper sang một model tương ứng 
	// json-databind đã hỗ trợ sẵn ObjectMapper.
	public <T> T toModel (Class<T> tClass)
	{
		try {
			return new ObjectMapper().readValue(value, tClass);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	// dữ liệu req.gereader() truyền vào là 1 chuỗi json do nó dã lấy được chuỗi
	// json từ client
	// và cần chuyển thành string.
	public static HttpUtil of(BufferedReader jsonString)
	{
		StringBuffer sb = new StringBuffer();
		try {
			String line;
			while ((line = jsonString.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new HttpUtil(sb.toString());
	}

}
