package com.oaikerblogit.util;

import javax.servlet.http.HttpServletRequest;

// nhiệm vụ : duy trì user (sử dụng session)
public class SessionUtil {

	private static SessionUtil sessionUtil = null;

	public static SessionUtil getInstance() {
		if (sessionUtil == null) {// nếu chưa được khỏi tạo thì khởi tạo
			sessionUtil = new SessionUtil();
		}
		return sessionUtil;
	}

	// put vào session :
	public void putValue(HttpServletRequest req, String key, Object value) {
		// đối tượng session được tạo ra từ HttpServletRequest (req.getSesstion)
		req.getSession().setAttribute(key, value);
	}

	// Object do không biết cụ thể kiểu dl
	public Object getValue(HttpServletRequest req, String key) {
		return req.getSession().getAttribute(key);
	}

	public void removeValue(HttpServletRequest req, String key) {
		req.getSession().removeAttribute(key);
	}

}
