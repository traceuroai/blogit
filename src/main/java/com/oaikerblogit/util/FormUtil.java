package com.oaikerblogit.util;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

/* chức năng : map tất cả param (giống httpUtil là chuyển chuỗi json thành 1 đối tượng) thành một đối tượng
 * cách làm : sử dụng beanUtils.popolate
 * 
 * ứng dụng : tối ưu bước req.getparameter từ url.
 * 
 * */

public class FormUtil {

	@SuppressWarnings({ "deprecation", "unchecked" })
	public static <T> T toModel(Class<T> tClass, HttpServletRequest req) {
		T object = null;
		try {
			object = tClass.newInstance();
			BeanUtils.populate(object, req.getParameterMap());// chú ý : các value của param có thể là một mảng (vd : name:oai,a,b...)
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			System.out.println(e.getMessage());
		}
		return object;
	}

}
