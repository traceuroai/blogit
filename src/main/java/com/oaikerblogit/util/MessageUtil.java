package com.oaikerblogit.util;

import javax.servlet.http.HttpServletRequest;

public class MessageUtil {

	public static void showMessage(HttpServletRequest req) {
		if (req.getParameter("message") != null) {
			String message = req.getParameter("message");
			String messageResponse = "";
			String alert = "";
			if (message.equals("insert_success")) {
				messageResponse = "Insert_success";
				alert = "success";
			} else if (message.equals("update_success")) {
				messageResponse = "Update_success";
				alert = "success";
			} else if (message.equals("delete_success")) {
				messageResponse = "Delete_success";
				alert = "success";
			} else if (message.equals("error_system")) {
				messageResponse = "Error_system";
				alert = "danger";
			}
			req.setAttribute("message", messageResponse);
			req.setAttribute("alert", alert);
		}
	}

}
