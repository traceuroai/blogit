package com.oaikerblogit.controller.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.constant.SystemConstant;
import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.service.INewService;

@WebServlet(urlPatterns = { "/home" })
public class homeController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/*
	 * để sử dụng được Inject : thêm thư viện, file beans.xml, filter trong web.xml
	 */

	@Inject
	private ICategoryService categoryService;

	@Inject
	private INewService newService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CategoryModel categoryModel = new CategoryModel();
		categoryModel.setListResult(categoryService.findAll());
		
		NewModel newModel = new NewModel();
		List<NewModel> listNew = new ArrayList<>();
		
		for(int i = 0; i < categoryModel.getListResult().size(); ++i)// lặp các thể loại đó
		{
			// lấy tất cả các bài viết thuộc từng thể loại rồi add vào listNew.
			newModel.setListResult(newService.findByCategoryId(categoryModel.getListResult().get(i).getId()));
			listNew.addAll(newModel.getListResult());
		}

		req.setAttribute("categorys", categoryModel.getListResult());
		req.setAttribute(SystemConstant.MODEL, listNew);
		// trả về trang chủ home
		RequestDispatcher rd = req.getRequestDispatcher("/views/web/home.jsp");
		rd.forward(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
