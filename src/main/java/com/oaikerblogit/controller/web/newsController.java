package com.oaikerblogit.controller.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.service.INewService;
import com.oaikerblogit.util.FormUtil;

@WebServlet(urlPatterns = {"/news"})
public class newsController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ICategoryService categoryService;
	
	@Inject
	private INewService newService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		NewModel newModel = FormUtil.toModel(NewModel.class, req);
		
		CategoryModel categoryModel = new CategoryModel();
		categoryModel.setListResult(categoryService.findAll());
		
		newModel = newService.findOneById(newModel.getId());

		req.setAttribute("categorys", categoryModel.getListResult());
		req.setAttribute("news", newModel);
		// trả về trang chủ home
		RequestDispatcher rd = req.getRequestDispatcher("/views/web/chiTiet.jsp");
		rd.forward(req, resp);
	}

}
