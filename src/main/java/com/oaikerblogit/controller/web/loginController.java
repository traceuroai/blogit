package com.oaikerblogit.controller.web;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.service.IUserService;
import com.oaikerblogit.util.FormUtil;
import com.oaikerblogit.util.SessionUtil;

@WebServlet(urlPatterns = { "/oaklogin", "/oaklogout" })
public class loginController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private IUserService userService;

	ResourceBundle resourceBundle = ResourceBundle.getBundle("message");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		if (action != null && action.equals("login")) {
			RequestDispatcher rd = req.getRequestDispatcher("views/login.jsp");
			rd.forward(req, resp);
		} else if (action != null && action.equals("logout")) {
			SessionUtil.getInstance().removeValue(req, "userModelSession");// xóa session khi logout
			resp.sendRedirect(req.getContextPath() + "/home");
		} else {
			resp.sendRedirect(req.getContextPath() + "/home");
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		if (action != null && action.equals("login")) {
			String message = req.getParameter("message");
			String alert = req.getParameter("alert");
			if (message != null && alert != null) {
				req.setAttribute("message", resourceBundle.getString(message));
				req.setAttribute("alert", resourceBundle.getString(alert));
			}
			UserModel userModel = FormUtil.toModel(UserModel.class, req);
			userModel = userService.findbyUserAndPasswordAndStatus(userModel.getUserName(), userModel.getPassword(), 1);
			if (userModel != null) {
				// nếu có rồi -> dùng lại còn không sẽ tạo mới SessionUtil
				SessionUtil.getInstance().putValue(req, "userModelSession", userModel);
				if (userModel.getRole().getCode().equals("USER")) {
					resp.sendRedirect("/home");
				} else if (userModel.getRole().getCode().equals("ADMIN")) {
					resp.sendRedirect("/admin67-home");
				}
			} else {
				resp.sendRedirect("/oaklogin?action=login&message=username_password_invalid&alert=danger");
			}
		}
	}

}
