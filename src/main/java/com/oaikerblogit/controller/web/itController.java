package com.oaikerblogit.controller.web;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.service.INewService;

@WebServlet(urlPatterns = {"/it-cuocsong"})
public class itController extends HttpServlet{
	
private static final long serialVersionUID = 1L;
	
	/*
	 * để sử dụng được Inject : thêm thư viện, file beans.xml, filter trong web.xml
	 */
	
	@Inject
	private ICategoryService categoryService;
	
	@Inject
	private INewService newService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<CategoryModel> categorys = categoryService.findAll();
		req.setAttribute("categorys", categorys);
		CategoryModel category = new CategoryModel();
		String ct = req.getRequestURI().substring(1);
		for (CategoryModel categoryModel : categorys) {
			if(categoryModel.getCode().equals(ct))
			{
				category = categoryService.findOneByCode(ct);
			}
		}
		List<NewModel> news = newService.findByCategoryId(category.getId());
		req.setAttribute("news", news);
		RequestDispatcher rd = req.getRequestDispatcher("/views/web/it.jsp");
		rd.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
	}

}
