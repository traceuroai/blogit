package com.oaikerblogit.controller.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.service.INewService;

@WebServlet(urlPatterns = {"/ve-toi"})
public class aboutmeController extends HttpServlet{
	
private static final long serialVersionUID = 1L;
	
	@Inject
	private INewService newService;
	
	@Inject
	private ICategoryService categoryService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		req.setAttribute("news", newService.findAll());
		req.setAttribute("categorys", categoryService.findAll());
		NewModel nm = new NewModel();
		nm.setListResult(newService.findByCategoryId(5L));
		// trả về trang chủ home
		req.setAttribute("news", nm);
		RequestDispatcher rd = req.getRequestDispatcher("/views/web/aboutme.jsp");
		rd.forward(req, resp);
	}

}
