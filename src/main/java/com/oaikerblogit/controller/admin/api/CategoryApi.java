package com.oaikerblogit.controller.admin.api;

import java.io.IOException;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.util.HttpUtil;
import com.oaikerblogit.util.SessionUtil;

@WebServlet(urlPatterns = { "/admin67-api-category" })
public class CategoryApi extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private ICategoryService categoryService;

//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		req.setCharacterEncoding("UTF-8");
//		resp.setContentType("application/json");
//		ObjectMapper ob = new ObjectMapper();
//		List<CategoryModel> listCategory = categoryService.findAll();
//		if (listCategory == null) {
//			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
//		}
//		ob.writeValue(resp.getOutputStream(), listCategory);
//	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		CategoryModel categoryModel = HttpUtil.of(req.getReader()).toModel(CategoryModel.class);
		categoryModel.setCreatedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		categoryModel = categoryService.insert(categoryModel);
		if (categoryModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), categoryModel);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		CategoryModel categoryModel = HttpUtil.of(req.getReader()).toModel(CategoryModel.class);
		categoryModel.setModifiedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		categoryModel = categoryService.update(categoryModel);
		if (categoryModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), categoryModel);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		CategoryModel categoryModel = HttpUtil.of(req.getReader()).toModel(CategoryModel.class);
		categoryService.delete(categoryModel.getIds());
		ob.writeValue(resp.getOutputStream(), "{}");
	}

}
