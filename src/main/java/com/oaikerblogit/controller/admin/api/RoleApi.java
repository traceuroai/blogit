package com.oaikerblogit.controller.admin.api;

import java.io.IOException;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oaikerblogit.model.RoleModel;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.service.IRoleService;
import com.oaikerblogit.util.HttpUtil;
import com.oaikerblogit.util.SessionUtil;

@WebServlet(urlPatterns = { "/admin67-api-role" })
public class RoleApi extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private IRoleService roleService;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		RoleModel roleModel = HttpUtil.of(req.getReader()).toModel(RoleModel.class);
		roleModel.setCreatedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		roleModel = roleService.insert(roleModel);
		if (roleModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), roleModel);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		RoleModel roleModel = HttpUtil.of(req.getReader()).toModel(RoleModel.class);
		roleModel.setModifiedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		roleModel = roleService.update(roleModel);
		if (roleModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), roleModel);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		RoleModel roleModel = HttpUtil.of(req.getReader()).toModel(RoleModel.class);
		roleService.delete(roleModel.getIds());
		ob.writeValue(resp.getOutputStream(), "{}");
	}

}
