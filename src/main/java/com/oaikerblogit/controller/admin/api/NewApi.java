package com.oaikerblogit.controller.admin.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.service.INewService;
import com.oaikerblogit.util.HttpUtil;
import com.oaikerblogit.util.SessionUtil;

@WebServlet(urlPatterns = { "/admin67-api-new" })

@MultipartConfig

public class NewApi extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String UPLOAD_DIR = "images";

	@Inject
	private INewService newService;
	
	private ServletContext context;

//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		req.setCharacterEncoding("UTF-8");
//		resp.setContentType("application/json");
//		ObjectMapper ob = new ObjectMapper();
//		List<NewModel> listNew = newService.findAll();
//		if (listNew == null) {
//			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
//		}
//		ob.writeValue(resp.getOutputStream(), listNew);
//	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		// convert từ json -> newModel.
		NewModel newModel = HttpUtil.of(req.getReader()).toModel(NewModel.class);
		newModel.setCreatedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());

		
//		String fileNames = "";
//		try {
//			Part parts = req.getParts();
//			String realPath = context.getRealPath(UPLOAD_DIR);
//			String fileName = Path.of(parts.getSubmittedFileName()).getFileName().toString();
//			if (!Files.exists(Path.of(realPath))) {
//				Files.createDirectory(Path.of(realPath));
//			}
//			parts.write(realPath + "/" + fileName);
//			fileNames = realPath + "/" + fileName;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		newModel.setThumbnail(fileNames);
		

		newModel = newService.insert(newModel);
		// chuyển thành kiểu json rồi trả về : json-databind đã hỗ trợ sẵn ObjectMapper.
		if (newModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), newModel);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		NewModel newModel = HttpUtil.of(req.getReader()).toModel(NewModel.class);
		newModel.setModifiedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		newModel = newService.update(newModel);
		if (newModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), newModel);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		NewModel newModel = HttpUtil.of(req.getReader()).toModel(NewModel.class);
		newService.delete(newModel.getIds());
		ob.writeValue(resp.getOutputStream(), "{}");
	}

}
