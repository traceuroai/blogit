package com.oaikerblogit.controller.admin.api;

import java.io.IOException;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.service.IUserService;
import com.oaikerblogit.util.HttpUtil;
import com.oaikerblogit.util.SessionUtil;

@WebServlet(urlPatterns = { "/admin67-api-user" })
public class UserApi extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private IUserService userService;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		UserModel userModel = HttpUtil.of(req.getReader()).toModel(UserModel.class);
		userModel.setCreatedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		userModel = userService.insert(userModel);
		if (userModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), userModel);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		UserModel userModel = HttpUtil.of(req.getReader()).toModel(UserModel.class);
		userModel.setModifiedBy(((UserModel) SessionUtil.getInstance().getValue(req, "userModelSession")).getFullName());
		userModel = userService.update(userModel);
		if (userModel == null) {
			resp.setStatus(HttpsURLConnection.HTTP_UNAUTHORIZED);
		}
		ob.writeValue(resp.getOutputStream(), userModel);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		ObjectMapper ob = new ObjectMapper();
		UserModel userModel = HttpUtil.of(req.getReader()).toModel(UserModel.class);
		userService.delete(userModel.getIds());
		ob.writeValue(resp.getOutputStream(), "{}");
	}

}
