package com.oaikerblogit.controller.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.constant.SystemConstant;
import com.oaikerblogit.model.CategoryModel;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.util.FormUtil;

@WebServlet(urlPatterns = { "/admin67-category" })
public class CategoryController extends HttpServlet {
	
	@Inject
	private ICategoryService categoryService;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CategoryModel categoryModel = FormUtil.toModel(CategoryModel.class, req);
		String view = "";
		if (categoryModel.getType().equals(SystemConstant.LIST)) {
			categoryModel.setListResult(categoryService.findAll());
			view = "/views/admin/category/list.jsp";
		} else if (categoryModel.getType().equals(SystemConstant.EDIT)) {
			if (categoryModel.getId() != null) {
				categoryModel = categoryService.findOneById(categoryModel.getId());
			}
//			req.setAttribute("categories", categoryService.findAll());
			view = "/views/admin/category/edit.jsp";
		}
		req.setAttribute(SystemConstant.MODEL, categoryModel);
		RequestDispatcher rd = req.getRequestDispatcher(view);
		rd.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
