package com.oaikerblogit.controller.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.constant.SystemConstant;
import com.oaikerblogit.model.NewModel;
import com.oaikerblogit.paging.PageRequest;
import com.oaikerblogit.paging.Pageble;
import com.oaikerblogit.service.ICategoryService;
import com.oaikerblogit.service.INewService;
import com.oaikerblogit.sort.Sorter;
import com.oaikerblogit.util.FormUtil;
import com.oaikerblogit.util.MessageUtil;

@WebServlet(urlPatterns = { "/admin67-new" })
public class NewController extends HttpServlet {

	@Inject
	private INewService newService;
	
	@Inject
	private ICategoryService categoryService;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*
		 * newModel là mắt xích trung gian cho việc trả ra view và nhận vào từ view ->
		 * build trường chung là 1 list (sử dụng generic AbstractModel) -> nhận được các
		 * giá trị tham số map vào newModel
		 */
		NewModel newModel = FormUtil.toModel(NewModel.class, req);// thực hiện việc chuyển chuỗi.(CHÚ Ý)

		/*
		 * String pageStr = req.getParameter("page"); String maxPageItemStr =
		 * req.getParameter("maxPageItem"); if (pageStr != null) {
		 * newModel.setPage(Integer.parseInt(pageStr)); } else { newModel.setPage(1); }
		 * if(maxPageItemStr != null) {
		 * newModel.setMaxPageItem(Integer.parseInt(maxPageItemStr)); }
		 */

		String view = "";
		if (newModel.getType().equals(SystemConstant.LIST)) {
			Pageble pageble = new PageRequest(newModel.getPage(), newModel.getMaxPageItem(),
					new Sorter(newModel.getSortName(), newModel.getSortBy()));
//			Integer offset = ((newModel.getPage() - 1) * newModel.getMaxPageItem());// offset : phần tử bắt đầu
			newModel.setListResult(newService.findAll(pageble));
			newModel.setTotalItem(newService.getTotalItem());
			newModel.setTotalPage((int) Math.ceil((double) newModel.getTotalItem() / newModel.getMaxPageItem()));
			view = "/views/admin/new/list.jsp";
		} else if (newModel.getType().equals(SystemConstant.EDIT)) {
			if (newModel.getId() != null) {// cập nhật
				newModel = newService.findOneById(newModel.getId());
			}
			req.setAttribute("categories", categoryService.findAll());
			view = "/views/admin/new/edit.jsp";
		}
		
		MessageUtil.showMessage(req);
		
		req.setAttribute(SystemConstant.MODEL, newModel);
		RequestDispatcher rd = req.getRequestDispatcher(view);
		rd.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
