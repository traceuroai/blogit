package com.oaikerblogit.controller.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.constant.SystemConstant;
import com.oaikerblogit.model.RoleModel;
import com.oaikerblogit.service.IRoleService;
import com.oaikerblogit.util.FormUtil;

@WebServlet(urlPatterns = { "/admin67-role" })
public class RoleController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private IRoleService roleService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RoleModel roleModel = FormUtil.toModel(RoleModel.class, req);

		String view = "";
		if (roleModel.getType().equals(SystemConstant.LIST)) {
			roleModel.setListResult(roleService.findAll());
			view = "/views/admin/role/list.jsp";
		} else if (roleModel.getType().equals(SystemConstant.EDIT)) {
			if (roleModel.getId() != null) {
				roleModel = roleService.findOneById(roleModel.getId());
			}
			req.setAttribute("roles", roleService.findAll());
			view = "/views/admin/role/edit.jsp";
		}
		req.setAttribute(SystemConstant.MODEL, roleModel);
		RequestDispatcher rd = req.getRequestDispatcher(view);
		rd.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
