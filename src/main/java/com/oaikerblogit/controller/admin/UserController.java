package com.oaikerblogit.controller.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oaikerblogit.constant.SystemConstant;
import com.oaikerblogit.model.UserModel;
import com.oaikerblogit.paging.PageRequest;
import com.oaikerblogit.paging.Pageble;
import com.oaikerblogit.service.IRoleService;
import com.oaikerblogit.service.IUserService;
import com.oaikerblogit.sort.Sorter;
import com.oaikerblogit.util.FormUtil;

@WebServlet(urlPatterns = { "/admin67-user" })
public class UserController extends HttpServlet {

	@Inject
	private IUserService userService;

	@Inject
	private IRoleService roleService;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		UserModel userModel = FormUtil.toModel(UserModel.class, req);

		String view = "";
		if (userModel.getType().equals(SystemConstant.LIST)) {
			Pageble pageble = new PageRequest(userModel.getPage(), userModel.getMaxPageItem(),
					new Sorter(userModel.getSortName(), userModel.getSortBy()));
			userModel.setListResult(userService.findAll(pageble));
			userModel.setTotalItem(userService.getTotalItem());
			userModel.setTotalPage((int) Math.ceil((double) userModel.getTotalItem() / userModel.getMaxPageItem()));
			view = "/views/admin/user/list.jsp";
		} else if (userModel.getType().equals(SystemConstant.EDIT)) {
			if (userModel.getId() != null) {
				userModel = userService.findOneById(userModel.getId());
			}
			req.setAttribute("roles", roleService.findAll());
			view = "/views/admin/user/edit.jsp";
		}
		req.setAttribute(SystemConstant.MODEL, userModel);
		RequestDispatcher rd = req.getRequestDispatcher(view);
		rd.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
